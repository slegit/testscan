import java.io.*;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class VulnerableServlet extends HttpServlet {
    
    // Vulnerable method with SQL injection and XSS vulnerabilities
    protected void doGet(HttpServletRequest request, HttpServletResponse response) 
            throws ServletException, IOException {
        
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        
        // Get user input from the request
        String userInput = request.getParameter("input");
        
        // Integrate user input directly into SQL query (vulnerable to SQL injection)
        String query = "SELECT * FROM products WHERE name = '" + userInput + "'";
        
        // Vulnerability: Eval function executes arbitrary code, leading to XSS vulnerability
        String xssVulnerableCode = "<script>eval('" + userInput + "');</script>";
        
        // Establish database connection
        String url = "jdbc:mysql://localhost:3306/mydatabase";
        String user = "root";
        String password = "password";
        
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection con = DriverManager.getConnection(url, user, password);
            Statement stmt = con.createStatement();
            
            // Execute the vulnerable SQL query
            ResultSet rs = stmt.executeQuery(query);
            
            out.println("<html><body>");
            out.println("<h2>Product Details</h2>");
            
            // Display query results
            while (rs.next()) {
                // Vulnerability: Displaying user input without proper HTML escaping (vulnerable to XSS)
                out.println("Name: " + rs.getString("name") + "<br>");
                out.println("Description: " + rs.getString("description") + "<br>");
            }
            out.println("<br>");
            out.println("<h2>XSS Vulnerable Code</h2>");
            // Vulnerability: Displaying user input without proper HTML escaping (vulnerable to XSS)
            out.println(xssVulnerableCode);
            out.println("</body></html>");
            
            // Close database resources
            rs.close();
            stmt.close();
            con.close();
        } catch (Exception e) {
            out.println("Error: " + e);
        }
    }
}
